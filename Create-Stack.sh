#!/bin/bash

echo '\n###############################\n\n#This script will create an AWS stack (no SSL) who deploy a "HelloWorld" application using AWS\n'
echo '#This example is created as code and none depeldencies are creates by hand\n'
echo '#New version are deployed if execute again this file\n\n###############################\n'


VPCID=$(aws ec2 describe-vpcs --filters "Name=isDefault, Values=true" --query 'Vpcs[0].VpcId' --output text)
echo  Default VpcId : ${VPCID}
CIDR=$(aws ec2 describe-vpcs --filters "Name=isDefault, Values=true" --query 'Vpcs[0].CidrBlockAssociationSet[0].CidrBlock' --output text)
echo  CIDR of default Vpc : ${CIDR}
SUBNETS=$(aws ec2 describe-subnets --filters "Name=default-for-az,Values=true"  --query 'Subnets[?MapPublicIpOnLaunch==`true`].SubnetId' --output json)
echo  Public Subnets of default VPC : ${SUBNETS}
KEYNAME=$(aws ec2 describe-key-pairs  --query 'KeyPairs[0].KeyName' --output text)
echo First KeyPair founded in AWS is :  ${KEYNAME}

# aws cloudformation create-stack --stack-name webstack \
#                       --template-body file://web-stack.yaml \
#                       --parameters ParameterKey=VpcId,ParameterValue="${VPCID}" ParameterKey=Subnets,ParameterValue="${SUBNETS}" ParameterKey=KeyName,ParameterValue="${KEYNAME}"    \
#                       --capabilities CAPABILITY_NAMED_IAM \
#                       --region eu-west-1 
