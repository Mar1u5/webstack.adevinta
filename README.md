# The aim of this repository is to create the definded :  [Web Stack File](./DevOpsTechnicalTest.pdf) 

## Requirements that were met

Resources that are created with [CloudFormation Template](./web-stack.yaml) . You can see more deetails in file.

1. SG for ALB
2. SG for EC2
3. LaunchConfig
   1. Delete EBS on termination
4. Autoscaling Group
   1. Notify to SNS topic ARN if there is any error en launching EC2 instances 
5. Aplication Load Balancer
6. Listenr for Aplication Load Balancer
7. Target Group
   1. HealthChecks configured
8. Roling Updates for Autoscaling
9. SNS topic
10. Install awslogs in every EC2
11. Create IAM role to allow pushing logs to CloudWatch
12. Install Nginx and set up a example file that return the instance id when the ALB dns is requested.
---

## TODO: Initial Requirements


1. Create the AutoSaling policies and alarms to scale in and out the application based in the number of ALB requests handled 
2. Set up files to push nginx logs to CW
3. Finalize the [script](./Create-Stack.sh) wich could allow read and pass values to the [CloudFormation Template](web-stack.yaml)
4. Implement creation of frendly records (example.com) in Route 53 that points ( Alias or A record type ) to the ALB DNS 
---

## TODO: More Requirements that we found necessary in this stack

1. Create a set of templates that allow to configure different types of stacks : node/jave/python/etc ...
2. Create a flag that allow to force the Autosclaing Roling Oupdate
3. Create a Mappings that allow to define multiple AMIs in diferent regions.
4. Create a Mappings that allow to deploy in Windwos or Linux Instances
5. Create a mechanism that allow deploy any Repository:
   1. Parameter that allow clone any Repo
   2. Parameter that allow to define the tech stack used and link them with the Point 1 (node/jave/python/etc ...)

---
